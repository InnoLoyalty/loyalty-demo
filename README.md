# demo/register

Demonstration of the use and customisation of the Innocard Loyalty registration snippet in the form of a 
fictual hotel website.

You can find this website deployed at https://register-demo.innocardloyaltytest.ch

